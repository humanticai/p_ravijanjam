'''
lock acquisition and release example when multiple corotuines use the same lock

date time stamp is put, show should be almost concurrent to within 1 sec and variations within milli seconds because we don't do a heavy compute operation in any of the coroutines
'''

import asyncio
import asyncio as asy
import random as rnd
import collections as cl
import datetime as dt
import time

i = 0
dq = cl.deque()

dtn = lambda : dt.datetime.now()


# insert into queue
async def fqa(l):

    global i, dq
    while True :

        async with l : 
            print("fqa:lock-aquired: ", dtn())
            i = i+1
            dq.appendleft(i)

        #print("fqa:{}".format(len(dq)))
        await asy.sleep(2*rnd.random())
        #await asy.sleep(1)#*rnd.random())
        print("fqa:lock-released: ", dtn())



# if signal ok, then pop, wait 1 sec, else don't do anything
async def fqc(l,e):

    global i, dq
    while True :

        await asy.sleep(1)
        async with l :
            print("fqc:lock-aquired: ", dtn())

        print("fqc:lock-released: ", dtn())

        '''
        async with l : 

            if e[0] == 1 : 
                e[0] = 0
                if len(dq) > 0 : 
                    dq.pop()
                    await asy.sleep(2)
                print("fqc-pop, ", len(dq))

            e[0] = 1

        #print("fqc:{}".format(len(dq)))
        #await asy.sleep(2.5*rnd.random())
        '''

async def fa():
    pass


def x():
    print("x")


async def f1(n):

    i = 0
    while True :
        i = i+1
        print("{}:{}".format(n,i))
        await asy.sleep(2*rnd.random())

        if i%5 == 0:
            #x()
            await f1(n+1)
            #t = asy.create_task(f1(n+1))
            #await t




async def main():

    l = asyncio.Lock()
    q = []
    e = [0,0]

    t1 = asy.create_task(f1(1))
    await t1


    #t1 = asy.create_task(fqa(l))
    #t1 = asy.create_task(fqa(l))
    #t2 = asy.create_task(fqc(l, e))

    #await t1, t2





if __name__ == "__main__":

    #loop.run_until_complete(main())
    loop = asyncio.get_event_loop()
    asyncio.ensure_future(main())
    loop.run_forever()
