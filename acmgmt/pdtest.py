import psycopg2 as pg
import pandas as pd

conn = None
try:
    conn = pg.connect("dbname='postgres' user='postgres' host='localhost' password='abc'")
except:
    print("I am unable to connect to the database")


cur = conn.cursor()
cur.execute("""SELECT datname from pg_database""")
rows = cur.fetchall()
print(rows)


# account table
q = f"""
CREATE TABLE IF NOT EXISTS linkedin_info(
  start timestamp,
  stop timestamp
);
"""
cur.execute(q)

q = r"""
insert into linkedin_info (start, stop)
values ('{0}', '{1}')
"""
print(q.format(2,3))


t = pd.Timestamp.now(tz="UTC").strftime('%Y-%m-%d %X.%f')
print(t)
q = q.format(t,t)
print(q)
cur.execute(q)


q = r"""
select * from linkedin_info
"""
cur.execute(q)
print(cur.fetchall())


q = r"""
select start, stop, EXTRACT(EPOCH FROM (start - stop)) AS cd
from linkedin_info
"""
cur.execute(q)
x = cur.fetchall()
for e in x[0] :
	print(e, " : ", type(e))





'''
# stats table
def choose_one(): pass
def add_account(): pass
def remove_account(): pass
def flush_table(): pass
def active_accounts(): pass
'''
