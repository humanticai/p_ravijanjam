import pgmain as pm
import pandas as pd


fts = lambda  : pd.Timestamp.now(tz="UTC").strftime('%Y-%m-%d %X.%f')

def f3():

	q = r"""
	select count(*) from T
	"""
	_, r = pm.qexec(q, sr=1)
	print("results: ", r)


def f0():

	q = r"""
	drop TABLE IF EXISTS T
	"""
	pm.qexec(q)



def f1():

	q = r"""
	CREATE TABLE IF NOT EXISTS T(
		x1 timestamp,
		x2 int
	)
	"""
	pm.qexec(q)



def f2():

	q = r"""
	insert into T(x1, x2)
		values('{0}', {1})
	"""
	q = q.format(fts(), 1)
	print(q)
	pm.qexec(q)


#f0()
f1()
f2()
f3()

