## understand the behavior of lock object and event object as their multiplicity increases when accessed from multiple coroutines

what does the code do, quick note

* a14.py : _stepped flow_, coroutine spawns another coroutine in a nested fashion without going back to where it started. A away to keep the previous one running concurrently is to be figured out. 
* a13.py : lock acquisition/release from same coroutine 
* a12.py : same as a11.py, q fills faster than popped out, but that’s because the consumer is waiting an additional 1-second regardless of it’s processing time. To remember, when a lock is there, no update happens anywhere. 
* a11.py : 1-queue, 1-consumer, timing very accurately adjusted, one lock, two locks might be needed, identify which lock does what 
* a10.py : if a lock is used two coroutines need to be present, single lock, just locks and a release is necessary or enable lock release manually. 
* a9.py : two coroutine, with lock updating the size of deque where main loop is not in an infinite loop
* a7.py : same as a6.py, just that deque size is shown instead of the entire deque
* a6.py : insert from one dq, and remove from another, deque lock
* a5.py : queue, lock, from two coroutines, where queue is updated 
* a4.py : lock, order out of randomly executing asynchronous functions tasks 
* a3.py : order out of randomly executing asynchronous functions tasks 


<br>
### how to run the code

no installations required, just plain python should do
```
python3 filename.py
```


<br>
the easiest way to keep live sessions is to spawn a fixed number of coroutines in the main event loop, instead of dynamically spawning them on demand and traffic management becomes simplified. It is to be figured out how to make a coroutine spawn coroutines to run concurrently with the parent coroutine
