import psycopg2 as pg


# get results after executing a query
def qgr(q) :

	conn = None
	try:
	    conn = pg.connect(database = "postgres", user = "postgres", password = "abc", host = "localhost", port = "5432")
	except:
	    print("I am unable to connect to the database") 


	cur = conn.cursor()
	try:
	    cur.execute(q)
	    rows = cur.fetchall()
	    print(rows)
	except:
	    print("I can't drop our test database!")

	conn.commit() # <--- makes sure the change is shown in the database
	conn.close()
	cur.close()



def qexec(q) :

	conn = None
	try:
	    conn = pg.connect(database = "postgres", user = "postgres", password = "abc", host = "localhost", port = "5432")
	except:
	    print("I am unable to connect to the database") 


	cur = conn.cursor()
	try:
	    cur.execute(q)
	except:
	    print("I can't drop our test database!")

	conn.commit() # <--- makes sure the change is shown in the database
	conn.close()
	cur.close()



q = r"""
CREATE TABLE IF NOT EXISTS linkedin_info(
  start timestamp,
  stop timestamp,
  time_diff float,
  url varchar,
  wait_until varchar,
  visit_count int,
  call_from varchar,
  status int
)
"""
qexec(q)




q = f"""
SELECT *
FROM pg_catalog.pg_tables
WHERE schemaname != 'pg_catalog' AND
    schemaname != 'information_schema';
"""
qgr(q)


