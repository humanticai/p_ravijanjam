import asyncio


async def f(i):
    print(i)
    await asyncio.sleep(1)
    i = i+1
    await f(i)


async def waiter(event):
    print('waiting for it ...')
    await event.wait()
    print('... got it!')
    await waiter(event)


async def main():

    await f(0)

    '''
    # Create an Event object.
    event = asyncio.Event()

    # Spawn a Task to wait until 'event' is set.
    waiter_task = asyncio.create_task(waiter(event))

    # Sleep for 1 second and set the event.
    await asyncio.sleep(1)
    if event.is_set() : event.clear()
    else : event.set()

    # Wait until the waiter task is finished.
    await waiter_task
    '''



if __name__ == "__main__":
    #loop.run_until_complete(main())
    loop = asyncio.get_event_loop()
    asyncio.ensure_future(main())
    loop.run_forever()
