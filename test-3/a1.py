import asyncio
import asyncio as asy
import random as rnd
import collections as cl

i = 0
dq = cl.deque()


# insert into queue
async def fqa(l):

    global i, dq
    while True :

        async with l : 
            i = i+1
            dq.appendleft(i)

        print("fqa:{}".format(len(dq)))
        await asy.sleep(2*rnd.random())
        #await asy.sleep(1)#*rnd.random())



# if signal ok, then pop, wait 1 sec, else don't do anything
async def fqc(l,e):

    global i, dq
    while True :

        await asy.sleep(1)
        async with l : 

            if e[0] == 1 : 
                e[0] = 0
                if len(dq) > 0 : 
                    dq.pop()
                    await asy.sleep(2)
                print("fqc-pop, ", len(dq))

            e[0] = 1

        #print("fqc:{}".format(len(dq)))
        #await asy.sleep(2.5*rnd.random())


async def main():

    l = asyncio.Lock()
    q = []
    e = [0,0]

    t1 = asy.create_task(fqa(l))
    t2 = asy.create_task(fqc(l, e))

    await t1, t2





if __name__ == "__main__":

    #loop.run_until_complete(main())
    loop = asyncio.get_event_loop()
    asyncio.ensure_future(main())
    loop.run_forever()
