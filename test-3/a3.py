import asyncio
import asyncio as asy
import random as rnd
import collections as cl
import pandas as pd
import pgmain as pm


i = 0
dq = cl.deque()

# utc time stamp generator
fts = lambda  : pd.Timestamp.now(tz="UTC").strftime('%Y-%m-%d %X.%f')

# random number generator
frni = lambda x=1, y=10 : rnd.randint(x, y)



async def dbf2():

	q = r"""
	select count(*) from T
	"""
	_, r = pm.qexec(q, sr=1)
	print("results: ", r)




async def dbf1(n):

	while True : 
		await asy.sleep(1)
		q = r"""
		insert into T(x1, x2)
			values('{0}', {1})
		"""
		q = q.format(fts(), frni())
		pm.qexec(q)
		await dbf2()



async def main():

    t1 = asy.create_task(dbf1(1))
    t2 = asy.create_task(dbf1(2))
    t3 = asy.create_task(dbf1(3))

    await t1, t2, t3





if __name__ == "__main__":

    #loop.run_until_complete(main())
    loop = asyncio.get_event_loop()
    asyncio.ensure_future(main())
    loop.run_forever()
