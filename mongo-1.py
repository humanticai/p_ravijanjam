#from pymongo import MongoClient
import pymongo as pym
import datetime
from datetime import timedelta
import pprint
import yaml


f = open('frrole_linkedin_scraper/config.yaml')
x = yaml.load(f, Loader=yaml.FullLoader)
print(x)

config = x


host = config['mongo']['prod']['hosts']
replicaSet = config['mongo']['prod']['replica_set']
username = config['mongo']['prod']['username']
password = config['mongo']['prod']['password']


def _connection(
    hosts,
    rs_name,
    username,
    password,
    auth_db='admin',
    mechanism='SCRAM-SHA-1',
    timeout=60 * 1000,
    keep_alive=True,
    ):
    conn = MongoClient(
        hosts,
        socketKeepAlive=keep_alive,
        replicaSet=rs_name,
        read_preference=ReadPreference.SECONDARY_PREFERRED,
        connect=True,
        socketTimeoutMS=timeout,
        )

    conn[auth_db].authenticate(username, password, mechanism=mechanism)
    print('Created a new mongo connection to RS: {rs}, Hosts: {hosts}'.format(rs=rs_name,
            hosts=hosts))
    return conn


client = pym.MongoClient('example.com', username='user', password='password', authSource='the_database', authMechanism='SCRAM-SHA-256')
#print(client.server_info())
db = client['linkedin_final']
print(db)
cl = db['profiles']
print(cl)
#print(db.list_collection_names())
print(cl.find_one())


#print(host, replicaSet, username, password)




