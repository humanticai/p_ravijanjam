import numpy as np

cc = 1

x = np.__version__
print("[{}], numpy version :{}".format(cc, x))


import boto3
x = boto3.__version__
cc+=1
print("[{}], boto3 version :{}".format(cc, x))
