import asyncio


async def waiter(event):
    print('waiting for it ...')
    await event.wait()
    print('... got it!')
    event.set()
    return waiter(event)


async def main():
    # Create an Event object.
    event = asyncio.Event()

    # Spawn a Task to wait until 'event' is set.
    waiter_task = asyncio.create_task(waiter(event))

    # Sleep for 1 second and set the event.
    await asyncio.sleep(1)
    if event.is_set() : event.clear()
    else : event.set()

    # Wait until the waiter task is finished.
    await waiter_task



if __name__ == "__main__":
    #loop.run_until_complete(main())
    loop = asyncio.get_event_loop()
    asyncio.ensure_future(main())
    loop.run_forever()
