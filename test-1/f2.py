# https://aiojobs.readthedocs.io/en/stable/quickstart.html

import asyncio
import aiojobs
import random as rnd

async def coro(n,timeout):

    print("coro:{}-{}: ".format(n, timeout))
    await asyncio.sleep(timeout*rnd.random())

async def main():

    scheduler = await aiojobs.create_scheduler()
    for i in range(4):
        # spawn jobs
        await scheduler.spawn(coro(i, i/10))

    await asyncio.sleep(5.0)
    # not all scheduled jobs are finished at the moment

    # gracefully close spawned jobs
    await scheduler.close()

asyncio.get_event_loop().run_until_complete(main())
