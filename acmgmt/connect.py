import psycopg2 as pg

conn = None
try:
    conn = pg.connect("dbname='postgres' user='postgres' host='localhost' password='abc'")
except:
    print("I am unable to connect to the database")


cur = conn.cursor()
cur.execute("""SELECT datname from pg_database""")
rows = cur.fetchall()
print(rows)


q = f"""
CREATE TABLE IF NOT EXISTS T3 (
  c1 timestamp,
  c2 timestamp
);

INSERT INTO T3 (c1, c2)
VALUES ('2016-06-21 15:10:25','2016-06-21 10:10:25' ),('2017-07-21 11:09:25','2017-07-21 10:09:25'),('2017-07-28 18:9:25','2017-07-28 17:09:25') ;
"""
cur.execute(q)


q = f"""
SELECT *
FROM pg_catalog.pg_tables
WHERE schemaname != 'pg_catalog' AND
    schemaname != 'information_schema';
"""
cur.execute(q)
rows = cur.fetchall()
print(rows)


q = f"""
CREATE TABLE IF NOT EXISTS linkedin_info(
  start timestamp,
  stop timestamp,
  time_diff float,
  url varchar,
  wait_until varchar,
  visit_count int,
  call_from varchar,
  status int
);

INSERT INTO T3 (c1, c2)
VALUES ('2016-06-21 15:10:25','2016-06-21 10:10:25' ),('2017-07-21 11:09:25','2017-07-21 10:09:25'),('2017-07-28 18:9:25','2017-07-28 17:09:25') ;
"""
cur.execute(q)


# account table
q = f"""
CREATE TABLE IF NOT EXISTS linkedin_info(
  start timestamp,
  stop timestamp,
  time_diff float,
  time_elapsed float, 
  url string,
  wait_until string,
  visit_count int,
  call_from string,
  status int
);
"""
cur.execute(q)


# stats table


def choose_one(): pass
def add_account(): pass
def remove_account(): pass
def flush_table(): pass
def active_accounts(): pass
