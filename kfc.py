'''
https://kafka-python.readthedocs.io/en/master/
'''

from kafka import KafkaConsumer
from kafka import TopicPartition


consumer = KafkaConsumer('my_favorite_topic')
for msg in consumer: print (msg)


# join a consumer group for dynamic partition assignment and offset commits
consumer = KafkaConsumer('my_favorite_topic', group_id='my_favorite_group')
for msg in consumer: print (msg)


# manually assign the partition list for the consumer
consumer = KafkaConsumer(bootstrap_servers='localhost:9092')
consumer.assign([TopicPartition('foobar', 2)])
msg = next(consumer)


# Deserialize msgpack-encoded values
consumer = KafkaConsumer(value_deserializer=msgpack.loads)
consumer.subscribe(['msgpackfoo'])
for msg in consumer:
    assert isinstance(msg.value, dict)




