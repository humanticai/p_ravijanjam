# https://aiojobs.readthedocs.io/en/stable/quickstart.html

import asyncio
import aiojobs
import random as rnd


def yf():
	cc = 1
	while True :
		cc = cc+1
		yield cc


async def coro(n,timeout):

    print("coro:{}-{}: ".format(n, timeout))
    #await asyncio.sleep(timeout*rnd.random())
    await asyncio.sleep(10*rnd.random())
    print("done:{}-{}: ".format(n, timeout), end="\n\n")
    


async def main():

    s = await aiojobs.create_scheduler(limit=2)
    await s.spawn(coro(1, 1/10))
    await s.spawn(coro(2, 2/10))
    await s.spawn(coro(3, 3/10))
    await s.spawn(coro(4, 4/10))

    #await asyncio.sleep(5.0)
    # not all scheduled jobs are finished at the moment

    # gracefully close spawned jobs
    await  s.close()


if __name__ == "__main__":
	l = asyncio.get_event_loop()
	l.run_until_complete(main())

#asyncio.get_event_loop().run_until_complete(main())
