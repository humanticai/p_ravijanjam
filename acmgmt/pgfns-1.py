import psycopg2 as pg
from psycopg2 import extensions
import yaml
import time


# connectivity to db
def getconn():

	conn = pg.connect(
	    dbname = "postgres",
	    user = "postgres",
	    host = "localhost",
	    password = "abc"
	)

	return conn



# define a function that returns the PostgreSQL connection status
# https://kb.objectrocket.com/postgresql/get-the-status-of-a-transaction-with-the-psycopg2-python-adapter-for-postgresql-745
def get_transaction_status(conn):

    # print the connection status
    print ("\nconn.status:", conn.status)

    # evaluate the status for the PostgreSQL connection
    if conn.status == extensions.STATUS_READY:
        print ("psycopg2 status #1: Connection is ready for a transaction.")

    elif conn.status == extensions.STATUS_BEGIN:
        print ("psycopg2 status #2: An open transaction is in process.")

    elif conn.status == extensions.STATUS_IN_TRANSACTION:
        print ("psycopg2 status #3: An exception has occured.")
        print ("Use tpc_commit() or tpc_rollback() to end transaction")

    elif conn.status == extensions.STATUS_PREPARED:
        print ("psycopg2 status #4:A transcation is in the 2nd phase of the process.")
    return conn.status


#get_transaction_status(getconn())


# create the table if doesn't exist, and report the same
# if not report it exists

def getres(q) : 

	conn = getconn()
	start_time = time.time()
	cursor = conn.cursor()
	cursor.execute(q)
	print(cursor.fetchall())
	cursor.close()
	conn.close()



# execute query 
def qexec(q):

	conn = getconn()
	start_time = time.time()

	cursor = None
	try:
	    cursor = conn.cursor()
	    cursor.execute(q)
	    #print(cursor.fetchall())

	except:

	    print ("TIME:", time.time() - start_time)

	    # get the poll status again
	    #check_poll_status()

	    # get transaction status AFTER
	    get_transaction_status(conn)

	    # rollback the previous transaction in order to make another
	    conn.rollback()

	    # make another SQL request to sleep
	    #cursor.execute("select pg_sleep(4)")

	    # get the poll status again
	    #check_poll_status()

	#cursor.commit()
	cursor.close()
	conn.close()

# remove the table
def droptable(tbl):
	qexec("drop table {}".format(tbl))
	

 
#q = "SELECT datname from pg_database"
#qexec(q)
