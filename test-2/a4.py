import asyncio
import asyncio as asy
import random as rnd

i = 0

async def waiter(event):

    print('waiting for it ...')
    await event.wait()
    print('... got it!')
    event.clear()

    #if event.is_set() : event.clear()
    #else : event.set()

    #await waiter(event)



async def g(lock):

    global i
    while True :

        async with lock : 
            i = i+1

        print("g:{}".format(i))
        await asy.sleep(5*rnd.random())



async def f(lock):

    global i
    while True :
        async with lock : 
            i = i+1

        print("f:{}".format(i))
        await asy.sleep(5*rnd.random())


async def fq1(q):

    q.append(1)
    q.append(2)
    print(q)


async def main():

    #global i 

    lock = asyncio.Lock()
    q = []

    #i = i+1
    #print(i)
    #await asyncio.sleep(1)

    t1 = asy.create_task(f(lock))
    t2 = asy.create_task(g(lock))
    t3 = asy.create_task(fq1(q))

    await t1
    await t2
    #await t3

    '''
    # Create an Event object.
    event = asyncio.Event()

    # Spawn a Task to wait until 'event' is set.
    waiter_task = asyncio.create_task(waiter(event))

    # Sleep for 1 second and set the event.
    await asyncio.sleep(1)
    #event.set()
    if event.is_set() : event.clear()
    else : event.set()

    # Wait until the waiter task is finished.
    await waiter_task
    '''



if __name__ == "__main__":

    #loop.run_until_complete(main())
    loop = asyncio.get_event_loop()
    asyncio.ensure_future(main())
    loop.run_forever()
