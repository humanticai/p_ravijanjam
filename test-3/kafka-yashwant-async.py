# stolen from yashwant

import asyncio
import aiojobs
import socket
import asyncio
from datetime import datetime
from kafka import KafkaProducer
import json
import random
sequential_processing = False
sequential_number_generation = False
prev_number = None


KAFKA_HOSTS = '10.21.4.100:9092'
KAFKA_CLIENT = socket.gethostname()
KAFKA_TOPIC_PRODUCER = "humantic_scaling_pipeline_test"


def init_kafka_client(hosts='10.21.4.100:9092'):
    return KafkaProducer(bootstrap_servers=hosts, api_version=(0, 10, 0))


def send_message(message_to_send):
    print(message_to_send)
    producer = init_kafka_client()
    producer.send(KAFKA_TOPIC_PRODUCER,str.encode(json.dumps(message_to_send)))
    producer.flush()


def get_kafka_message():
    global sequential_processing, sequential_number_generation, prev_number
    print("Prev Number: {0}".format(prev_number))
    if sequential_number_generation:
        if prev_number is None:
            rand_number = random.randint(0, 100)
            prev_number = rand_number
        number = (prev_number + 1) % 100
        prev_number = number
        kafka_msg_to_send = {
            'number': number,
            'time':  datetime.utcnow().isoformat()
        }
        return kafka_msg_to_send
    else:
        number = (random.randint(0, 100)) % 100
        prev_number = number
        kafka_msg_to_send = {
            'number': number,
            'time': datetime.utcnow().isoformat()
        }
        return kafka_msg_to_send


async def random_sequential_function():
    global sequential_processing
    while True:
        rand_sleep = random.randint(0, 200)
        print("Sequential Sleep: {0}".format(rand_sleep))
        await asyncio.sleep(rand_sleep)
        if sequential_processing:
            sequential_processing = False
        else:
            sequential_processing = True


async def random_sequential_number_function():
    global sequential_number_generation
    while True:
        rand_sleep = random.randint(0, 50)
        print("Sequential Number Sleep: {0}".format(rand_sleep))
        await asyncio.sleep(rand_sleep)
        if sequential_number_generation:
            sequential_number_generation = False
        else:
            sequential_number_generation = True


async def main():
    global sequential_processing
    scheduler = await aiojobs.create_scheduler(limit=2)
    await scheduler.spawn(random_sequential_function())
    await scheduler.spawn(random_sequential_number_function())
    while True:
        while sequential_processing:
            kafka_msg_to_send = get_kafka_message()
            send_message(kafka_msg_to_send)
            await asyncio.sleep(2)
        rand_sleep = random.randint(0, 20)
        print("Processing Sleep: {0}".format(rand_sleep))
        await asyncio.sleep(rand_sleep)
        kafka_msg_to_send = get_kafka_message()
        send_message(kafka_msg_to_send)

loop = asyncio.get_event_loop()
loop.run_until_complete(main())
