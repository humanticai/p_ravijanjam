import psycopg2 as pg
from psycopg2 import extensions
import yaml
import time
import pgfns as pf
import pandas as pd


def a1():
	# query for creating table
	q = f"""
	CREATE TABLE IF NOT EXISTS linkedin_info (
	  start timestamp,
	  stop timestamp,
	  time_diff float,
	  url varchar,
	  wait_until varchar,
	  visit_count int,
	  call_from varchar,
	  status int
	)
	"""
	print(q)
	pf.qexec(q)


#pf.droptable('linkedin_info')
#a1()


q = "create table if not exists T (a varchar)"
pf.qexec(q)


print("list all tables")
q = f"""
SELECT *
FROM pg_catalog.pg_tables
WHERE schemaname != 'pg_catalog' AND
    schemaname != 'information_schema';
"""

q = "select * from T"
#pf.getres(q)
print("\n")


def a2():

	q = r"""
	INSERT INTO linkedin_info(
	  start, stop, time_diff, url, wait_until,visit_count, call_from, status
	  ) 
	  VALUES
	  ({0});
	"""
	df = pd.read_csv('test-data.csv')
	cols = df.columns
	nc = len(cols)

	ss = None
	for k, e in df.iterrows():
		s = ''
		for m in range(0,nc-1):
			print(e[m])
			s += '{},'.format(e[m])	

		s += '{}'.format(e[nc-1])	
		ss = s
		q = q.format(ss)
		print(q)
		pf.qexec(q)
		
		break


#q = "select count(*) from linkedin_info"
#pf.qexec(q)
