import psycopg2 as pg


# check connection status, just the connection, no cursor related things
def conncheck() :

	conn = None
	try:
	    conn = pg.connect(database = "postgres", user = "postgres", password = "abc", host = "localhost", port = "5432")
	except:
	    return -1, None

	conn.close()
	return 0, None



'''
execute a list of queries sequentially and get the final result
input : 
  q : list of queries, [q1, q2, q3, ...], qk: query string
sr : 
   0 - just execute the query
   1 - show the results as well

exit codes:
 -1 : connection failure
 -2 : some cursor related problem 
  1 : execution success, but rows not populated, (1, None)
  2 : rows are returnd, (2, rows)
  3 : neither of ( 1, 2 ), something else happend which lead to successful execution of query, (3, None)
'''
def mqexec(q, sr=0) :

	conn = None
	try:
	    conn = pg.connect(database = "postgres", user = "postgres", password = "abc", host = "localhost", port = "5432")
	except:
	    return -1, None


	cur = conn.cursor()
	rows = None
	try:
	    for e in q : 
	    	cur.execute(e)

	    if(sr == 1): rows = cur.fetchall()
	except:
            return -2, None

	conn.commit() # <--- makes sure the change is shown in the database
	conn.close()
	cur.close()
	if rows == None : return 1, None
	else : return 2, rows

	return 3, None



'''
execute a query
sr : 
   0 - just execute the query
   1 - show the results as well

exit codes:
 -1 : connection failure
 -2 : some cursor related problem 
  1 : execution success, but rows not populated, (1, None)
  2 : rows are returnd, (2, rows)
  3 : neither of ( 1, 2 ), something else happend which lead to successful execution of query, (3, None)
'''
def qexec(q, sr=0) :

	conn = None
	try:
	    conn = pg.connect(database = "postgres", user = "postgres", password = "abc", host = "localhost", port = "5432")
	except:
	    return -1, None


	cur = conn.cursor()
	rows = None
	try:
	    cur.execute(q)
	    if(sr == 1): rows = cur.fetchall()
	except:
            return -2, None

	conn.commit() # <--- makes sure the change is shown in the database
	conn.close()
	cur.close()
	if rows == None : return 1, None
	else : return 2, rows

	return 3, None





'''
# tests 
q = r"""
CREATE TABLE IF NOT EXISTS linkedin_info(
  start timestamp,
  stop timestamp,
  time_diff float,
  url varchar,
  wait_until varchar,
  visit_count int,
  call_from varchar,
  status int
)
"""
qexec(q)




q = f"""
SELECT *
FROM pg_catalog.pg_tables
WHERE schemaname != 'pg_catalog' AND
    schemaname != 'information_schema';
"""
_, r = qexec(q, sr=1)
print(r)


q = f"""
select version()
"""
_, r = qexec(q, sr=1)
print(r)
'''



