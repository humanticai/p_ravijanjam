import asyncio
import asyncio as asy
import random as rnd
import collections as cl

i = 0
dq = cl.deque()


async def fe():
    while True :
        e = rnd.choice([[0,0],[0,1],[0,1],[1,1]])
        print(e)



async def fq1(l,e):

    global i, dq
    while True :

        async with l : 
            i = i+1
            dq.appendleft(i)

        print("fq1:{}".format(len(dq)))
        await asy.sleep(2*rnd.random())



async def fq2(l,e):

    global i, dq
    while True :

        async with l : 
            dq.pop()

        print("fq2:{}".format(len(dq)))
        await asy.sleep(2.5*rnd.random())



async def waiter(event):

    print('waiting for it ...')
    await event.wait()
    print('... got it!')
    event.clear()

    #if event.is_set() : event.clear()
    #else : event.set()

    #await waiter(event)



async def g(lock):

    global i
    while True :

        async with lock : 
            i = i+1

        print("g:{}".format(i))
        await asy.sleep(5*rnd.random())



async def f(lock):

    global i
    while True :
        async with lock : 
            i = i+1

        print("f:{}".format(i))
        await asy.sleep(2*rnd.random())



'''
async def fq1(q, l):

    global i
    async with l : 
        i = i+1
        q.append(i)
        print("1:", q)



async def fq2(q, l):

    global i
    async with l : 
        i = i+1
        q.append(i)
        print("2:", q)
'''


async def main():

    l = asyncio.Lock()
    q = []
    e = [0,0]

    #te = asy.create_task(fe())

    while True:

        t3 = asy.create_task(fq1(l, e))
        t4 = asy.create_task(fq2(l, e))

        await t3
        await asy.sleep(1)
        await t4
        await asy.sleep(1)



if __name__ == "__main__":

    #loop.run_until_complete(main())
    loop = asyncio.get_event_loop()
    asyncio.ensure_future(main())
    loop.run_forever()
