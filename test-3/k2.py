from kafka.admin import KafkaAdminClient, NewTopic


admin_client = KafkaAdminClient(
    bootstrap_servers="localhost:9092", 
    client_id='test'
)


# create topics
def ct():
	topic_list = []
	topic_list.append(NewTopic(name="T2", num_partitions=1, replication_factor=1))
	topic_list.append(NewTopic(name="T3", num_partitions=1, replication_factor=1))
	topic_list.append(NewTopic(name="T4", num_partitions=1, replication_factor=1))
	admin_client.create_topics(new_topics=topic_list, validate_only=False)


# delete topics
# url : https://dev.to/sats268842/create-and-delete-kafka-topic-using-python-3ni
topic_names = ["T4"]
admin_client.delete_topics(topics=topic_names)
