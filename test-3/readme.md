


<br>
* `a3.py` : pg ins from multiple-coros

<br>
* `a2.py` : coroutinification of pg insertion only, pgmain reqd, single coro

<br>
* `pg1.py` : postgres T(x1, x2) basic fns for table mgmg fns


<br>
## admin
* table disc terminate event loop, all evt loops, if retry should go somewhere

<br>
* `k3.py` : code to get the list of consumer groups

<br>
* `k2.py` : code for kafka add, and delete producer, just run with python

<br>
* `k1` : kafka producer, just change the flags at the very top, and run using `python k1.py`
