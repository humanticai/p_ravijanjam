'''
stolen from yashwant
'''

import socket
from datetime import datetime
from kafka import KafkaProducer
import json
import threading
import random
import time
sequential_processing = False
sequential_number_generation = False
prev_number = None


KAFKA_HOSTS = '10.21.4.100:9092'
KAFKA_CLIENT = socket.gethostname()
KAFKA_TOPIC_PRODUCER = "humantic_scaling_pipeline_test"


# initalizing kafka client (input: ip address and port, output: kafkaproducer)
def init_kafka_client(hosts='10.21.4.100:9092'):
    return KafkaProducer(bootstrap_servers=hosts, api_version=(0, 10, 0))


# send the message to kafka (input: json object to send)
def send_message(message_to_send):
    print(message_to_send)
    producer = init_kafka_client()
    producer.send(KAFKA_TOPIC_PRODUCER,str.encode(json.dumps(message_to_send)))
    producer.flush()


# this funcition return number in a dependent fashion on a variable : sequential_number_generation.
# if sequential_number_generation is true, returns number in sequential manner like 1, 2, 3 else randomly
def get_kafka_message():
    global sequential_number_generation, prev_number
    print("Prev Number: {0}".format(prev_number))
    if sequential_number_generation:
        if prev_number is None:
            rand_number = random.randint(0, 100)
            prev_number = rand_number
        number = (prev_number + 1) % 100
        prev_number = number
        kafka_msg_to_send = {
            'number': number,
            'time':  datetime.utcnow().isoformat()
        }
        return kafka_msg_to_send
    else:
        number = (random.randint(0, 100)) % 100
        prev_number = number
        kafka_msg_to_send = {
            'number': number,
            'time': datetime.utcnow().isoformat()
        }
        return kafka_msg_to_send


# this function is responsible to change the variable sequential_processing to true/false which
# will be responsible for sending the messages to kafka either in sequential or random manner
def random_sequential_function():
    global sequential_processing
    while True:
        rand_sleep = random.randint(0, 200)
        print("Sequential Sleep: {0}".format(rand_sleep))
        time.sleep(rand_sleep)
        if sequential_processing:
            sequential_processing = False
        else:
            sequential_processing = True


# this function is responsible to change the variable sequential_number_generation to true/false which
# will be responsible for generating the number either in sequential or random pattern
def random_sequential_number_function():
    global sequential_number_generation
    while True:
        rand_sleep = random.randint(0, 50)
        print("Sequential Number Sleep: {0}".format(rand_sleep))
        time.sleep(rand_sleep)
        if sequential_number_generation:
            sequential_number_generation = False
        else:
            sequential_number_generation = True


#this functions generates kafka messages and send that to send_message function to send it to a queue
def main():
    global sequential_processing
    random_sequential_thread = threading.Thread(target=random_sequential_function, name="SequentialThread")
    random_sequential_thread.start()
    random_sequential_number_thread = threading.Thread(target=random_sequential_number_function, name="SequentialNumberThread")
    random_sequential_number_thread.start()
    while True:
        while sequential_processing:
            kafka_msg_to_send = get_kafka_message()
            send_message(kafka_msg_to_send)
            time.sleep(2)
        rand_sleep = random.randint(0, 20)
        print("Processing Sleep: {0}".format(rand_sleep))
        time.sleep(rand_sleep)
        kafka_msg_to_send = get_kafka_message()
        send_message(kafka_msg_to_send)


main()
