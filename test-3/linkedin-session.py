'''
src : https://bitbucket.org/humanticai/frrole_linkedin_scraper/src/master/linkedin_core/LinkedINSession.py
'''

import requests
import re
import json


class LinkedINSession:
    LINKEDIN_HOME = "https://www.linkedin.com/"
    LINKEDIN_LOGIN = "https://www.linkedin.com/uas/login"
    LINKEDIN_LOGIN_SUBMIT = "https://www.linkedin.com/checkpoint/lg/login-submit"

    def __init__(self, username: str = 'mukut@frrole.com', password: str = "Ciafo.Frrole"):
        self.session = requests.session()
        self.__prepare_session(username, password)

    def _change_useragent(self):
        """
        Currently using only one user-agent, will add couple more later
        """
        self.session.headers.update(
            {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.13; rv:56.0) Gecko/20100101 Firefox/56.0',
             'accept-encoding': 'gzip, deflate, sdch, br'
             })

    def __csrf_token(self, raw_html):
        """
        Extract CSRF token from the login form, this will also validate the login page fetch
        :param raw_html:
        :return: csrf_token:str
        """
        try:
            token = re.search(r'type="hidden" name="loginCsrfParam" value="([\w-]+)', raw_html).group(1)
            return token
        except Exception:
            raise Exception("Something wrong the linkedin login html form, unable to find the csrf token")

    def __prepare_session(self, username: str, password: str):
        """
        This function uses username and password to login and updates session obj
        :param username: linkedin Username:str
        :param password: linkedin password:str
        """
        login_payload = {"session_key": username,
                         "session_password": password,
                         "isJsEnabled": False,
                         "loginCsrfParam": self.__csrf_token(self._fetch_data(self.LINKEDIN_LOGIN, "get",
                                                                              voyager_api=False).text)
                         }

        login_request = requests.Request("POST", self.LINKEDIN_LOGIN_SUBMIT, data=login_payload)
        login_request.headers.update({'Referer': 'https://www.linkedin.com/',
                                      'Host': 'www.linkedin.com',
                                      'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
                                      'Content-Type': 'application/x-www-form-urlencoded',
                                      })

        prepped_request = self.session.prepare_request(login_request)
        response = self.session.send(prepped_request)

        # if there is valid login, then there is one redirect in response history
        try:
            assert len(response.history) == 1
        except Exception:
            raise Exception("Something wrong with login, redirect check failed")

    def _fetch_data(self, url: str, method: str = 'get', voyager_api=True, **kwargs):

        request_obj = requests.Request(method, url)
        # print(url)
        if voyager_api:
            # if 'page_instance_id' not in kwargs:
            #     raise Exception("Need Page Instance Id to make linkedin voyager api calls")

            request_obj.headers.update({'Csrf-Token': self.session.cookies.get("JSESSIONID")[1:-1],
                                       'x-li-lang': 'en_US',
                                       # 'x-li-page-instance': 'urn:li:page:d_flagship3_background;' + kwargs['page_instance_id'],
                                       'x-li-track': json.dumps(
                                           {"clientVersion": "1.2.281", "osName": "web", "timezoneOffset": 5.5,
                                            "deviceFormFactor": "DESKTOP", "mpName": "voyager-web"}),
                                       'x-restli-protocol-version': '2.0.0'
                                       })

        if method != 'get':
            raise Exception("Change here to support POST method by adding data to the request obj")

        prepped_request = self.session.prepare_request(request_obj)
        response_obj = self.session.send(prepped_request)
        # print(response_obj.status_code)
        return response_obj


if __name__ == "__main__":
    obj = LinkedINSession()
