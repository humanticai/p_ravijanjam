import asyncio as asy
import random as rnd

def f1():
    for e in range(0, 10):
        print(rnd.randint(0,10))

#f1()


async def fT(n, p):

    for e in range(0,n):
        await asy.sleep(rnd.randint(0,2)) 
        print('{}:{}'.format(p, e))


async def nested(n, p):

    asy.create_task(fT(20,3))

    for e in range(0,n):
        await asy.sleep(rnd.randint(0,2)) 
        print('{}:{}'.format(p, e))


async def main():

    t1 = asy.create_task(nested(20, 1))
    t2 = asy.create_task(nested(20, 2))

    await t1
    await t2
    await t2

asy.run(main())
