import pandas as pd
import yaml
import time
import pgmain as pm


def removedb():
	q = "drop table if exists linkedin_info"
	pm.qexec(q)


def createdb():

	q = r"""
	CREATE TABLE IF NOT EXISTS linkedin_info(
	  start timestamp,
	  stop timestamp,
	  time_diff float,
	  url varchar,
	  wait_until varchar,
	  visit_count int,
	  call_from varchar,
	  status int
	)
	"""
	pm.qexec(q)


def a0():

	q = f"""
	SELECT *
	FROM pg_catalog.pg_tables
	WHERE schemaname != 'pg_catalog' AND
	    schemaname != 'information_schema'; """
	_, r = pm.qexec(q, sr=1)
	print(r)



def a1():

	q = r"""
	select count(*) from linkedin_info
	"""
	_, r = pm.qexec(q, sr=1)
	print("results: ", r)



# insert data from csv
def a2():

	qy = r"""
	INSERT INTO linkedin_info(
	  start, stop, time_diff, wait_until,visit_count, call_from, status
	  ) 
	  VALUES
	  ({0});
	"""
	df = pd.read_csv('test-data.csv')
	cols = df.columns
	nc = len(cols)

	ss = None
	for k, e in df.iterrows():
		s = ''
		for m in range(0,nc-1):
			print(e[m])
			s += '{},'.format(e[m])	

		s += '{}'.format(e[nc-1])	
		ss = s
		q = qy.format(ss)
		#print(q)
		pm.qexec(q)
		


# diff with respect to current time computed externally
def a3():

	q = r"""
	select EXTRACT(EPOCH FROM ('{0}'-stop)) AS cd
	from linkedin_info
	"""
	tn = pd.Timestamp.now(tz="UTC").strftime('%Y-%m-%d %X.%f')
	q = q.format(tn)
	print(q)
	_, r = pm.qexec(q, sr=1)
	print("results: ", r)



# min & diff with respect to current time computed externally
def a4():

	q = r"""
	select MIN(EXTRACT(EPOCH FROM ('{0}'-stop))) AS cd
	from linkedin_info
	"""
	tn = pd.Timestamp.now(tz="UTC").strftime('%Y-%m-%d %X.%f')
	q = q.format(tn)
	_, r = pm.qexec(q, sr=1)
	print("results: ", r)



# min & diff with respect to current time computed externally
# operate and get the minimal value but only based on time diff only disregarding counts
def a5():

	q = r"""
	select call_from, MIN(EXTRACT(EPOCH FROM ('{0}'-stop))) AS cd
	from linkedin_info
	group by call_from
	order by cd desc
	"""
	tn = pd.Timestamp.now(tz="UTC").strftime('%Y-%m-%d %X.%f')
	q = q.format(tn)
	_, r = pm.qexec(q, sr=1)
	print(r)
	'''
	if len(r) > 0 :
		pass
		#print("results: ", r)
	'''


# multiple queries and one output using "mqexec"
def a6():

	q = [
	"drop view if exists v1",
	"create view v1 as select * from linkedin_info as L", 
	"select count(*) from v1"
	]
	_, r = pm.mqexec(q, sr=1)
	print("mq: ", r)



# multiple queries and one output using "mqexec"
def a7():

	q = [
	"drop view if exists v1",
	"create view v1 as select * from linkedin_info as L", 
	"select * from v1"
	]
	_, r = pm.mqexec(q, sr=1)
	print("mq: ", r)


removedb()
createdb()
a2()
#a0()
a1()
a3()
a4()
a5()
#a7()
		

#q = "select count(*) from linkedin_info"
#pf.qexec(q)
