import psycopg2 as pg


# connect to postgres database
def pgconct():

	conn = None
	try:
	    conn = pg.connect("dbname='postgres' user='postgres' host='localhost' password='abc'")
	except:
	    print("I am unable to connect to the database")
	    return -1, None

	return 0, conn



# get the cursor
def getcur():

	_, conn = pgconct()
	cur = conn.cursor()
	return 0, cur


# list all databases
def getdbs():

	_, cur = getcur()
	cur.execute("""SELECT datname from pg_database""")
	rows = cur.fetchall()
	return 0, rows
	

#_, rows = getdbs()


# get all the tables in the database
def gettbls():

	q = f"""
	SELECT *
	FROM pg_catalog.pg_tables
	WHERE schemaname != 'pg_catalog' AND
	    schemaname != 'information_schema';
	"""
	_, cur = getcur()
	cur.execute(q)
	rows = cur.fetchall()
	#cur.close()

	return 0, rows



# create the table for linkedin
def create_linkedintbl():
	
	_, cur = getcur()
	q = f"""
	CREATE TABLE IF NOT EXISTS linkedin_info(
	  start timestamp,
	  stop timestamp,
	  time_diff float,
	  url varchar,
	  wait_until varchar,
	  visit_count int,
	  call_from varchar,
	  status int
	)
	"""
	cur.execute(q)
	cur.close()
	

	
q = "drop table if exists linkedin_info"
_, cur = getcur()
cur.execute(q)
cur.close()



q = r"""
CREATE TABLE IF NOT EXISTS linkedin_info(
  start timestamp,
  stop timestamp,
  time_diff float,
  url varchar,
  wait_until varchar,
  visit_count int,
  call_from varchar,
  status int
)
"""

_, cur = getcur()
cur.execute(q)
cur.close()



_, rows = gettbls()
print(rows)

conn = None
try:
    conn = pg.connect(database = "postgres", user = "postgres", password = "abc", host = "localhost", port = "5432")
except:
    print("I am unable to connect to the database") 

cur = conn.cursor()
try:
    cur.execute("CREATE TABLE test (id serial PRIMARY KEY, num integer, data varchar);")
except:
    print("I can't drop our test database!")

conn.commit() # <--- makes sure the change is shown in the database
conn.close()
cur.close()
_, rows = gettbls()
print(rows)




#cur.close()


'''
create_linkedintbl()
ec, tbls = gettbls()
if(ec == 0):
	print(tbls)
'''
#cur.close()
#conn.close()


